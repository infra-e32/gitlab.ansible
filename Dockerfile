FROM ubuntu:22.04

ENV TZ=Europe/Moscow

RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
RUN apt-get -qy update
RUN apt install -qy python3 python3-pip software-properties-common git ansible sshpass
RUN pip install ansible-lint==4.0.0

CMD ["/bin/bash"]